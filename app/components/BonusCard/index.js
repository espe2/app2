import React, { Children } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Img from '../Img';
import Button from '../Button';
import A from '../A';

import { MdKeyboardArrowRight } from 'react-icons/md';

const CardHolder = styled.div`
  border: 1px #1E2A3C solid; 
  padding:2px 2px;
  background:#152335;
  border-radius:8px;
  margin:0px 18px 18px 18px;
  flex: 1;
  overflow:hidden;
  font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;
  cursor:pointer;
  img{
    width:100%;
    border-radius:8px;
  }
  &:hover {
    background: #132030;
  }
`;

const InfoBox = styled.div`
  padding:12px 12px 12px 12px;
  display: flex;
  flex: 1;
  flex-direction: row;
  justify-content: flex-start;
  a{
    width:100%;
    text-align:center;
  }
  div{
    width:100%;
  }
`;

const Title = styled.span`
  font-size:16px;
  color:#FFF;
  flex:1;
`;

const SubTitle = styled.span`
  font-size:14px;
  flex:1;
  color:#FFF;
  text-align:center;
  p{
    font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif !important;
    font:size:14px;
    color:#A4A4A4;
  }
`;


const Live = styled.div`
  width:12px;
  height:12px;
  background-color:#169D3A;
  border-radius:50%;
  float:right;
  margin-bottom:-42px;
  margin-right:22px;
`;

const ImageContainer = styled.div`
  border-radius:8px;
  text-align:center;
  margin:2px;
  img{
    height:50px;
    width:auto;
    margin:28px auto;
  }
`;



function BonusCard(props) {
  return (
    <CardHolder>
      <ImageContainer style={{background: props.color}}>
        <Img src={props.image} alt="Image"></Img>
      </ImageContainer>
      <InfoBox style={{paddingTop:0}}>
        <SubTitle><p>Bonus</p><span style={{color:"#FFB700"}}>€{props.bonus}</span></SubTitle>
        <SubTitle><p>Wager</p>{props.wager}x</SubTitle>
      </InfoBox>
      <InfoBox>
        <Button style={{flex:1}} href={props.url}>Claim <MdKeyboardArrowRight style={{fontSize:"18px"}}></MdKeyboardArrowRight></Button>
      </InfoBox>
      <InfoBox>
        <SubTitle style={{fontSize:"7px"}}>18+ | New Customers Only | Gamble Responsibly | <A href="https://begambleaware.org">www.begambleaware.org</A> | <A href={props.url}>T&C Apply</A></SubTitle>
      </InfoBox>
    </CardHolder>
  );
}

BonusCard.propTypes = {
  name: PropTypes.string.isRequired,
  bonus: PropTypes.string.isRequired,
  wager: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired
};


export default BonusCard;
