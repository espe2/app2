import React, { Children } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Img from '../Img';
import Button from '../Button';

import { MdKeyboardArrowRight } from 'react-icons/md';

const CardHolder = styled.div`
  border: 1px #1E2A3C solid; 
  padding:2px 2px;
  background:#152335;
  border-radius:8px;
  margin:0px 18px;
  flex: 1;
  overflow:hidden;
  font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;
  cursor:pointer;
  img{
    width:100%;
    border-radius:8px;
  }
  &:hover {
    background: #132030;
  }
`;

const InfoBox = styled.div`
  padding:12px 12px 12px 12px;
  display: flex;
  flex: 1;
  flex-direction: row;
  justify-content: flex-start;

`;

const Title = styled.span`
  font-size:16px;
  color:#FFF;
  flex:1;
`;

const SubTitle = styled.span`
  font-size:12px;
  flex:1;
  color:#FFF;
  p{
    font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif !important;
    font:size:14px;
    color:#A4A4A4;
  }
`;


const Live = styled.div`
  width:12px;
  height:12px;
  background-color:#169D3A;
  border-radius:50%;
  float:right;
  margin-bottom:-42px;
  margin-right:22px;
`;



function Card(props) {
  var modeLabel = '';
  switch(props.gameMode){
    case 'score':
      modeLabel = "Highest Score"
    break;
    case 'multiplier':
      modeLabel = "Highest Multiplier"
    break;
    case 'bonus':
      modeLabel = "Bonus Buys"
    break;
  }
  return (
    <CardHolder>
      <Live></Live>
      <Img src={props.image} alt="Image"></Img>
      <InfoBox>
        <Title>{props.title}</Title>
        <Button style={{flex:1}} href="/tournament/starburst/">Join <MdKeyboardArrowRight style={{fontSize:"18px"}}></MdKeyboardArrowRight></Button>
      </InfoBox>
      <InfoBox style={{paddingTop:0}}>
        <SubTitle><p>Prize Pool</p><span style={{color:"#FFB700"}}>€{props.prizePool}</span></SubTitle>
        <SubTitle><p>Game Mode</p>{modeLabel}</SubTitle>
      </InfoBox>
    </CardHolder>
  );
}

Card.propTypes = {
  title: PropTypes.string.isRequired,
  prizePool: PropTypes.string.isRequired,
  gameMode: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
};


export default Card;
