import styled from 'styled-components';

const Input = styled.input`
  height:45px;
  padding:6px 12px;
  background-color:#1B2738;
  border-radius:12px; 
  border: 1px #1E2A3C solid;
  min-width:270px;
  font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;

  ::placeholder{
    color:#FFB700;
    font-size:12px;
  }
`;

export default Input;
