import styled from 'styled-components';

const Item = styled.div`
    padding:18px 18px;
    color:#FFF;
    font-size:25px;
    text-align:center;

    &:hover{
        color:#FFB700;
        background:#1B2738;
    }
`;

export default Item;
