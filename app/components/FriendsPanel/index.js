import React from 'react';
import PropTypes from 'prop-types';


import Menu from './Menu';
import Item from './Item';
import Wrapper from './Wrapper';
import Img from '../Img';
import { GiThreeFriends } from 'react-icons/gi';
import HeadLiner from '../HeadLiner';

function FriendsPanel() {
  return (
    <Wrapper>
        <Menu>
          <HeadLiner><GiThreeFriends style={{fontSize:"34px", marginRight:"6px"}}></GiThreeFriends> 12</HeadLiner>
        </Menu>
    </Wrapper>
  );
}

export default FriendsPanel;
