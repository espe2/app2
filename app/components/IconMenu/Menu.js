import styled from 'styled-components';

const Menu = styled.div`
    flex-direction: column;
    display: flex;
    flex: 1;
    color:#FFF;
`;

export default Menu;
