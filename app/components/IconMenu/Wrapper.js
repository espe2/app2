import styled from 'styled-components';

const Wrapper = styled.div`
  width:80px;
  border-right: 1px #1E2A3C solid; 
  display:flex;
`;

export default Wrapper;
