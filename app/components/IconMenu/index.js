import React from 'react';
import PropTypes from 'prop-types';


import Menu from './Menu';
import Item from './Item';
import Wrapper from './Wrapper';
import { FiMenu, FiBell, FiMessageCircle, FiMoon, FiSettings } from 'react-icons/fi';

function IconMenu() {
  return (
    <Wrapper>
        <Menu>
          <Item><FiMenu></FiMenu></Item>
          <Item><FiBell></FiBell></Item>
          <Item><FiMessageCircle></FiMessageCircle></Item>
          <Item><FiMoon></FiMoon></Item>
          <Item><FiSettings></FiSettings></Item>
        </Menu>
    </Wrapper>
  );
}

export default IconMenu;
