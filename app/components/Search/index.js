/**
 *
 * Img.js
 *
 * Renders an image, enforcing the usage of the alt="" tag
 */

import React from 'react';
import PropTypes from 'prop-types';
import Input from './Input'
import { FiSearch } from 'react-icons/fi';

function Search(props) {
  return (
    <div style={{width:"100%"}}>
      <Input type="text" className={props.className} placeholder={props.placeholder} alt={props.alt} />
      <FiSearch style={{color:"#FFB700", margin:"14px 0 0 -32px"}}></FiSearch>
    </div>
  );
}

// We require the use of src and alt, only enforced by react in dev mode
Search.propTypes = {
  placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
  className: PropTypes.string,
};

export default Search;
