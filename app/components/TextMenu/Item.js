import styled from 'styled-components';
import React, { Children } from 'react';
import PropTypes from 'prop-types';

const ItemUI = styled.a`
  padding:18px 18px;
  color:#A4A4A4;
  font-size:14px;
  font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;
  text-decoration:none !important;
  :hover{
    color:#FFF;
    border-left:2px solid #FFD736;
    background:#1B2738;
  }
  &.active{
    color:#FFF;
    border-left:2px solid #FFD736;
    background:#1B2738;
  }
`;

function Item(props){
  return (
    <ItemUI className={props.className} href={props.href}>{Children.toArray(props.children)}</ItemUI>
  )
}


Item.propTypes = {
  href: PropTypes.string,
  children: PropTypes.node.isRequired,
  className: PropTypes.string
};

export default Item;
