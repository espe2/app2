import styled from 'styled-components';

export default styled.div`
  border: 1px #1E2A3C solid;
  height:80px;
  display: flex;
  flex: 1;
  flex-direction: row;
  justify-content: flex-start;
  img{
    width:100%;
  }
  div{
    display:flex;
  }
`;
