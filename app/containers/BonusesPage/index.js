/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React, { useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';

import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import {
  makeSelectRepos,
  makeSelectLoading,
  makeSelectError,
} from 'containers/App/selectors';
import HeadLiner from 'components/HeadLiner';
import BonusCard from 'components/BonusCard';
import GameCard from 'components/GameCard';
import Deck from 'components/Deck';
import { loadRepos } from '../App/actions';
import { changeUsername } from './actions';
import { makeSelectUsername } from './selectors';
import reducer from './reducer';
import saga from './saga';

import { GiLaurelsTrophy } from 'react-icons/gi';

GiLaurelsTrophy

const key = 'home';

const ActivityCard = styled.div`
  background:#1A2739;
  border:1px #1E2A3C solid;
  padding:16px;
  color:#FFF;
  border-radius:8px;
  font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;
  margin:18px 18px;

  p{
    font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;
  }
`;


export function BonusesPage({
  username,
  loading,
  error,
  repos,
  onSubmitForm,
  onChangeUsername,
}) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
    // When initial state username is not null, submit the form to load repos
    if (username && username.trim().length > 0) onSubmitForm();
  }, []);

  const reposListProps = {
    loading,
    error,
    repos,
  };

  return (
    <article style={{flex:"1"}}>
      <Helmet>
        <title>Top Casino Bonuses</title>
        <meta
          name="description"
          content="A React.js Boilerplate application homepage"
        />
      </Helmet>
      <HeadLiner>Top Casino Bonuses</HeadLiner>
      <Deck>
        <BonusCard url="http://google.com" color="#fd5c3c" name="LeoVegas" wager="35" bonus="200% + €100" image="https://assets-global.website-files.com/5b3a9e4571762f1623a3a198/5b3a9e4571762f9f5da3a7c7_leo.png"></BonusCard>
        <BonusCard url="http://google.com" color="#3fbebb" name="Casumo" wager="35" bonus="100% + 50 FS" image="https://assets-global.website-files.com/5b3a9e4571762f1623a3a198/5b3a9e4571762febb2a3a7a1_casumo.png"></BonusCard>
        <BonusCard url="http://google.com" color="#000000" name="Dunder" wager="35" bonus="200% + 250 FS" image="https://assets-global.website-files.com/5b3a9e4571762f1623a3a198/5b3a9e4571762f6fa2a3a7a9_dunder.png"></BonusCard>
      </Deck>
      <HeadLiner>Other Casino Bonuses</HeadLiner>
      <Deck>
        <BonusCard url="http://google.com" color="#fd5c3c" name="Calzone" wager="35" bonus="200% + 250 FS" image="https://assets-global.website-files.com/5b3a9e4571762f1623a3a198/5b3a9e4571762f9f5da3a7c7_leo.png"></BonusCard>
        <BonusCard url="http://google.com" color="#3fbebb" name="Casumo" wager="35" bonus="100% + 50 FS" image="https://assets-global.website-files.com/5b3a9e4571762f1623a3a198/5b3a9e4571762febb2a3a7a1_casumo.png"></BonusCard>
        <BonusCard url="http://google.com" color="#fd5c3c" name="LeoVegas" wager="35" bonus="200% + 250 FS" image="https://assets-global.website-files.com/5b3a9e4571762f1623a3a198/5b3a9e4571762f9f5da3a7c7_leo.png"></BonusCard>
        <BonusCard url="http://google.com" color="#3fbebb"  name="Casumo" wager="35" bonus="100% + 50 FS" image="https://assets-global.website-files.com/5b3a9e4571762f1623a3a198/5b3a9e4571762febb2a3a7a1_casumo.png"></BonusCard>
      </Deck>
      <Deck>
        <BonusCard url="http://google.com" color="#fd5c3c" name="LeoVegas" wager="35" bonus="200% + 250 FS" image="https://assets-global.website-files.com/5b3a9e4571762f1623a3a198/5b3a9e4571762f9f5da3a7c7_leo.png"></BonusCard>
        <BonusCard url="http://google.com" color="#3fbebb" name="Casumo" wager="35" bonus="100% + 50 FS" image="https://assets-global.website-files.com/5b3a9e4571762f1623a3a198/5b3a9e4571762febb2a3a7a1_casumo.png"></BonusCard>
        <BonusCard url="http://google.com" color="#fd5c3c" name="LeoVegas" wager="35" bonus="200% + 250 FS" image="https://assets-global.website-files.com/5b3a9e4571762f1623a3a198/5b3a9e4571762f9f5da3a7c7_leo.png"></BonusCard>
        <BonusCard url="http://google.com" color="#3fbebb" name="Casumo" wager="35" bonus="100% + 50 FS" image="https://assets-global.website-files.com/5b3a9e4571762f1623a3a198/5b3a9e4571762febb2a3a7a1_casumo.png"></BonusCard>
      </Deck>
    </article>
  );
}

BonusesPage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  repos: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
  onSubmitForm: PropTypes.func,
  username: PropTypes.string,
  onChangeUsername: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  repos: makeSelectRepos(),
  username: makeSelectUsername(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

export function mapDispatchToProps(dispatch) {
  return {
    onChangeUsername: evt => dispatch(changeUsername(evt.target.value)),
    onSubmitForm: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(loadRepos());
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(BonusesPage);
