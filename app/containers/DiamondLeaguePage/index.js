/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React, { useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';

import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import {
  makeSelectRepos,
  makeSelectLoading,
  makeSelectError,
} from 'containers/App/selectors';
import HeadLiner from 'components/HeadLiner';
import Card from 'components/Card';
import GameCard from 'components/GameCard';
import Deck from 'components/Deck';
import { GiLaurelsTrophy } from 'react-icons/gi';
import { loadRepos } from '../App/actions';
import { changeUsername } from './actions';
import { makeSelectUsername } from './selectors';
import reducer from './reducer';
import saga from './saga';

GiLaurelsTrophy;

const key = 'home';

const ActivityCard = styled.div`
  background: #1a2739;
  border: 1px #1e2a3c solid;
  padding: 16px;
  color: #fff;
  border-radius: 8px;
  font-family: 'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans',
    'Helvetica Neue', Arial, sans-serif;
  margin: 18px 18px;

  p {
    font-family: 'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans',
      'Helvetica Neue', Arial, sans-serif;
  }
`;

export function DiamondLeague({
  username,
  loading,
  error,
  repos,
  onSubmitForm,
  onChangeUsername,
}) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
    // When initial state username is not null, submit the form to load repos
    if (username && username.trim().length > 0) onSubmitForm();
  }, []);

  const reposListProps = {
    loading,
    error,
    repos,
  };

  const DoubleCard = styled.div`
    flex: 1;
    flex-direction: column;
    display: flex;
  `;

  return (
    <article style={{ flex: '1' }}>
      <Helmet>
        <title>Coin League</title>
        <meta
          name="description"
          content="A React.js Boilerplate application homepage"
        />
      </Helmet>
      <HeadLiner>Coin League</HeadLiner>
      <Deck>
        <GameCard
          title="Wild Tornado"
          image="https://i.imgur.com/V6Lqdzo.png"
        />
        <GameCard title="Wishmaster" image="https://i.imgur.com/KqjXxqm.png" />
      </Deck>
      <Deck>
        <GameCard
          title="Berry Burst Max"
          image="https://i.imgur.com/0blSVFZ.png"
        />
        <GameCard
          title="Divine Fortune"
          image="https://i.imgur.com/b5oVbfY.png"
        />
        <GameCard title="Gonzo Quest" image="https://i.imgur.com/4v3wYXb.png" />
      </Deck>
      <Deck>
        <GameCard
          title="Jungle Spirit"
          image="https://i.imgur.com/CfeDwec.png"
        />
        <GameCard
          title="Koi Princess"
          image="https://i.imgur.com/kQ6ZIIb.png"
        />
        <GameCard title="Narcos" image="https://i.imgur.com/7GBQRUM.png" />
        <GameCard title="Reel Rush" image="https://i.imgur.com/VukN0g6.png" />
      </Deck>
    </article>
  );
}

DiamondLeague.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  repos: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
  onSubmitForm: PropTypes.func,
  username: PropTypes.string,
  onChangeUsername: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  repos: makeSelectRepos(),
  username: makeSelectUsername(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

export function mapDispatchToProps(dispatch) {
  return {
    onChangeUsername: evt => dispatch(changeUsername(evt.target.value)),
    onSubmitForm: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(loadRepos());
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(DiamondLeague);
