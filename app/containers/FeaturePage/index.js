/*
 * FeaturePage
 *
 * List all the features
 */
import React from 'react';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';

import HeadLiner from '../../components/HeadLiner';
import H1 from '../../components/H1';
import H3 from '../../components/H3';
import Img from '../../components/Img';

const ContentWrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  justify-content: flex-start;
`;

const TabWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  text-align:center;

  div{
    padding:6px;
  }
`;

const SlimCard = styled.div`
  background:#1A2739;
  border:1px #1E2A3C solid;
  padding:16px;
  color:#FFF;
  border-radius:8px;
  font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;
  margin:6px;

  p{
    font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;
  }
`;

const Message = styled.div`
  flex-direction: row;
  display: flex; 
  padding:8px 16px;
  font-size:12px;
  img{
    width:100%;
  }
`;


import { FiUsers } from 'react-icons/fi';
import { IoIosTrophy } from 'react-icons/io';

export default class FeaturePage extends React.Component {

  constructor(props){
    super(props);
    this.openTab = "chat";
  }

  render() {
    return(
    <article style={{flex:"1"}}>
      <Helmet>
        <title>Feature Page</title>
        <meta
          name="description"
          content="Feature page of React.js Boilerplate application"
        />
      </Helmet>
      <HeadLiner>Starburst €2000</HeadLiner>
      <ContentWrapper>
        <iframe style={{border:0, width:"900px", height:"500px", flex:1}} src="https://golden-staticnj.casinomodule.com/games/starburst_mobile_html/game/starburst_mobile_html.xhtml?lobbyURL=https%3A%2F%2Fnj-casino.goldennuggetcasino.com%2F&server=https%3A%2F%2Fgolden-game.casinomodule.com%2F&sessId=DEMO1470942351256-86640-EUR&operatorId=default&gameId=starburst_mobile_html_sw&lang=en&integration=standard&keepAliveURL=&defaultAudio=true"></iframe>
        <div style={{flexDirection: "column", display: "flex", width:"320px"}}>
          <SlimCard>
            <H1 style={{textAlign:"center", margin:"2px"}}>00:16:00</H1>
          </SlimCard>
          <TabWrapper>
            <SlimCard style={{flex:1, borderBottomColor: (this.openTab == "chat" ? "#FFB700" : "#1E2A3C")}} onClick={ this.openTab = "chat" }>
              <FiUsers style={{fontSize:"18px", marginTop:"-4px"}}></FiUsers> Chat
            </SlimCard>
            <SlimCard style={{flex:1, borderBottomColor: (this.openTab == "leaderboard" ? "#FFB700" : "#1E2A3C")}} onClick={this.openTab = "leaderboard"}>
              <IoIosTrophy style={{fontSize:"18px", marginTop:"-4px"}}></IoIosTrophy> Leaderboard
            </SlimCard>
          </TabWrapper>
          <div style={{display: (this.openTab == "chat" ? "block" : "none")}}>
            <SlimCard style={{padding:"3px"}}>
              <Message>
                <div style={{width:"32px", textAlign:"left"}}>
                  <Img src="https://www.voanews.com/themes/custom/voa/images/Author__Placeholder.png"></Img>
                </div>
                <div style={{flex:1, textAlign:"center", paddingTop:"6px"}}>
                  SirPontusSpins
                </div>
                <div style={{flex:1, textAlign:"center"}}>
                  Hey how are you guys doing today?
                </div>
              </Message>
              <Message>
                <div style={{width:"32px", textAlign:"left"}}>
                  <Img src="https://www.voanews.com/themes/custom/voa/images/Author__Placeholder.png"></Img>
                </div>
                <div style={{flex:1, textAlign:"center", paddingTop:"6px"}}>
                  Ogge21
                </div>
                <div style={{flex:1, textAlign:"center"}}>
                  Im doing fine, you? GL today everyone
                </div>
              </Message>
              <Message>
                <div style={{width:"32px", textAlign:"left"}}>
                  <Img src="https://www.voanews.com/themes/custom/voa/images/Author__Placeholder.png"></Img>
                </div>
                <div style={{flex:1, textAlign:"center", paddingTop:"6px"}}>
                  SirPontusSpins
                </div>
                <div style={{flex:1, textAlign:"center"}}>
                  nice to hear, me as well. GL
                </div>
              </Message>
              
            </SlimCard>
          </div>
          <div style={{display: (this.openTab == "leaderboard" ? "block" : "none")}}>
          <SlimCard style={{flexDirection: "row", display: "flex"}}>
            <div style={{flex:1, textAlign:"center"}}>
              <b>Spins</b><br/>
              <span style={{color:"#FFB700"}}>250</span>
            </div>
            <div style={{flex:1, textAlign:"center"}}>
              <b>Placement</b><br/>
              <span style={{color:"#FFB700"}}>532</span>
            </div>
            <div style={{flex:1, textAlign:"center"}}>
              <b>Points</b><br/>
              <span style={{color:"#FFB700"}}>0</span>
            </div>
          </SlimCard>
          <SlimCard style={{flexDirection: "row", display: "flex", padding:"8px 16px", fontSize:"12px"}}>
             <div style={{flex:1, textAlign:"left"}}>
              <b>Player</b>
            </div>
            <div style={{flex:1, textAlign:"center"}}>
              <b>Spins</b>
            </div>
            <div style={{flex:1, textAlign:"center"}}>
              <b>Points</b>
            </div>
            <div style={{flex:1, textAlign:"center"}}>
              <b></b>
            </div>
          </SlimCard>
          <SlimCard style={{flexDirection: "row", display: "flex", padding:"8px 16px", fontSize:"12px"}}>
             <div style={{flex:1, textAlign:"left"}}>
              #1 Ponna87
            </div>
            <div style={{flex:1, textAlign:"center"}}>
              0
            </div>
            <div style={{flex:1, textAlign:"center"}}>
              4300
            </div>
            <div style={{flex:1, textAlign:"center", color:"#FFB700"}}>
              €250
            </div>
          </SlimCard>
          <SlimCard style={{flexDirection: "row", display: "flex", padding:"8px 16px", fontSize:"12px"}}>
             <div style={{flex:1, textAlign:"left"}}>
              #2 melle12
            </div>
            <div style={{flex:1, textAlign:"center"}}>
              0
            </div>
            <div style={{flex:1, textAlign:"center"}}>
              2603
            </div>
            <div style={{flex:1, textAlign:"center", color:"#FFB700"}}>
              €100
            </div>
          </SlimCard>
          <SlimCard style={{flexDirection: "row", display: "flex", padding:"8px 16px", fontSize:"12px"}}>
             <div style={{flex:1, textAlign:"left"}}>
              #3 peterStuf
            </div>
            <div style={{flex:1, textAlign:"center"}}>
              0
            </div>
            <div style={{flex:1, textAlign:"center"}}>
              1623
            </div>
            <div style={{flex:1, textAlign:"center", color:"#FFB700"}}>
              €50
            </div>
          </SlimCard>
          </div>
        </div>
      </ContentWrapper>
      <HeadLiner>Starburst Slot Review</HeadLiner>
      <SlimCard>
      <H3>Theme and Story Line</H3>
      <p>Starburst does not appear to have much in the way of story or theme. The symbols are slot machine staples: lucky sevens, bar symbols, and a selection of gems.</p>

      <p>The background suggests a night sky or outer space. It is all very bright and upbeat.</p>

      <H3>Graphics, Sounds and Animations</H3>
      <p>The tone of the slot machine is very cheerful, the electronic soundtrack is a little drifty. The background is animated with gently drifting stars and the symbols on the reels seem to hang in a purple space-like void.</p>

      <p>The visuals are all very high def, so much that they can slow your browser a bit as if the software is struggling to keep up.</p>

      <p>The design is full of little details, gem symbols are all animated to twinkle, and you can scroll over the winlines to make them visible.</p>
      </SlimCard>
    </article>
    );
    };
}
