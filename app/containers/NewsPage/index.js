/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React, { useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';

import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import {
  makeSelectRepos,
  makeSelectLoading,
  makeSelectError,
} from 'containers/App/selectors';
import HeadLiner from 'components/HeadLiner';
import Card from 'components/Card';
import NewsCard from 'components/NewsCard';
import Deck from 'components/Deck';
import { GiLaurelsTrophy } from 'react-icons/gi';
import { loadRepos } from '../App/actions';
import { changeUsername } from './actions';
import { makeSelectUsername } from './selectors';
import reducer from './reducer';
import saga from './saga';

GiLaurelsTrophy;

const key = 'home';

const ActivityCard = styled.div`
  background: #1a2739;
  border: 1px #1e2a3c solid;
  padding: 16px;
  color: #fff;
  border-radius: 8px;
  font-family: 'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans',
    'Helvetica Neue', Arial, sans-serif;
  margin: 18px 18px;

  p {
    font-family: 'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans',
      'Helvetica Neue', Arial, sans-serif;
  }
`;

export function News({
  username,
  loading,
  error,
  repos,
  onSubmitForm,
  onChangeUsername,
}) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
    // When initial state username is not null, submit the form to load repos
    if (username && username.trim().length > 0) onSubmitForm();
  }, []);

  const reposListProps = {
    loading,
    error,
    repos,
  };

  const DoubleCard = styled.div`
    flex: 1;
    flex-direction: column;
    display: flex;
  `;

  return (
    <article style={{ flex: '1' }}>
      <Helmet>
        <title>News</title>
        <meta
          name="description"
          content="A React.js Boilerplate application homepage"
        />
      </Helmet>
      <HeadLiner>News</HeadLiner>
      <Deck>
        <NewsCard
          title="Growing fast and rapidly, extending..."
          image="https://activeops.com/wp-content/uploads/2017/01/ActiveOps-0276-752x500.jpg"
        />
        <NewsCard
          title="First local game station, close to you..."
          image="https://lh3.googleusercontent.com/proxy/hOyJC_ZoCh4W30YwJMfDUKXaYSZHzXdI17ucuZd3CznqW0qdNL3ggUul4-ypcgCg2V1DC8mVvlPWx6VWEtPIAMWCHoU11_np49J_d7NTXpblowuO9DM1VOAY4uBC_lNmFBF9bgvXqA"
        />
        <NewsCard
          title="Release 1.0 with a lot new features..."
          image="https://activeops.com/wp-content/uploads/2017/01/ActiveOps-0276-752x500.jpg"
        />
      </Deck>
    </article>
  );
}

News.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  repos: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
  onSubmitForm: PropTypes.func,
  username: PropTypes.string,
  onChangeUsername: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  repos: makeSelectRepos(),
  username: makeSelectUsername(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

export function mapDispatchToProps(dispatch) {
  return {
    onChangeUsername: evt => dispatch(changeUsername(evt.target.value)),
    onSubmitForm: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(loadRepos());
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(News);
